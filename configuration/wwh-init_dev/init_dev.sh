#!/bin/sh

sleep 10

GATEWAY_ID="313333371a004733"
DEVICE_EUI="036b7539dd631e1c"
DEVICE_APP_KEY="7e848ea5bf2bb85eb36277054e13f597"

OUTPUT=$(curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json'  -d '{"email":"admin", "password":"admin"}' 'localhost:8080/api/internal/login')

jwt=$(echo $OUTPUT |   sed -e 's/.*jwt":"//' -e 's/".*//' -e 's/,//g')

#echo $jwt
#exit

#jwt=$(echo "$OUTPUT" |   sed -n 's|.*"jwt":"\([^"]*\)".*|\1|p' )

OUTPUT=$(curl -X POST --header 'Content-Type:application/json' --header 'Accept: application/json' --header 'Grpc-Metadata-Authorization: Bearer '$jwt -d '{"apiKey":{"id":"string","isAdmin":true,"name":"WWH-Key"}}' 'localhost:8080/api/internal/api-keys' )

jwtToken=$(echo $OUTPUT  |   sed -e 's/.*jwtToken":"//' -e 's/".*//' -e 's/,//g')

#echo $jwtToken

#
# Add Network-Server
OUTPUT=$(curl -X POST --header 'Content-Type:application/json' --header 'Accept: application/json' --header 'Grpc-Metadata-Authorization: Bearer '$jwtToken -d '{  "networkServer": {    "name": "networkserver",    "server": "chirpstack-network-server:8000"  } }' 'localhost:8080/api/network-servers' )

# Add Service Profil
OUTPUT=$(curl -X POST --header 'Content-Type:application/json' --header 'Accept: application/json' --header 'Grpc-Metadata-Authorization: Bearer '$jwtToken -d '{  "serviceProfile": {    "name": "wildweide-service",    "organizationID": "1",    "networkServerID": "1", "addGWMetaData": true  } }' 'localhost:8080/api/service-profiles' )

serviceProfileID=$(echo $OUTPUT  |   sed -e 's/.*id":"//' -e 's/".*//' -e 's/,//g')


# Add Gateway
OUTPUT=$(curl -X POST --header 'Content-Type:application/json' --header 'Accept: application/json' --header 'Grpc-Metadata-Authorization: Bearer '$jwtToken -d '{  "gateway": {    "description": "WWH-Gateway",    "discoveryEnabled": true,  "location": {  "accuracy": 0,     "altitude": 0,  "latitude": 0,     "longitude": 0,      "source": "UNKNOWN"    },   "metadata": {},    "name": "WWH-Gateway",   "id":"'$GATEWAY_ID'",    "networkServerID": "1",    "organizationID": "1"  } }' 'localhost:8080/api/gateways' )
#echo ${OUTPUT}

# Add Application
OUTPUT=$(curl -X POST --header 'Content-Type:application/json' --header 'Accept: application/json' --header 'Grpc-Metadata-Authorization: Bearer '$jwtToken -d '{  "application": {    "description": "WWH-Application",    "name": "WWH-Application",    "serviceProfileID":"'$serviceProfileID'",    "organizationID": "1"  } }' 'localhost:8080/api/applications' )

applicationID=$(echo $OUTPUT  |   sed -e 's/.*id":"//' -e 's/".*//' -e 's/,//g')

applicationID=1
#echo $applicationID

#Add Device-Profile
OUTPUT=$(curl -X POST --header 'Content-Type:application/json' --header 'Accept: application/json' --header 'Grpc-Metadata-Authorization: Bearer '$jwtToken -d '{ "deviceProfile": {  "geolocBufferTTL": 0,    "geolocMinBufferSize": 0,  "name": "WWH-Device-profile",   "macVersion" : "1.0.3",  "networkServerID": "1",    "organizationID": "1",   "rxDROffset1": 0,  "rxDataRate2": 0,   "rxDelay1": 0,    "rxFreq2": 0,  "payloadCodec": "CUSTOM_JS", "payloadDecoderScript":"function Decode(fPort, bytes, variables) {  if (fPort == 10){ return {\"fence_voltage\":(parseInt(String.fromCharCode(bytes[0],bytes[1]))*100) }} else if (fPort == 20){ return {\"latitude\":((parseFloat(String.fromCharCode(bytes[0],bytes[1]))/100)+51.03563), \"longitude\":((parseFloat(String.fromCharCode(bytes[2],bytes[3]))/100)+13.73447) } } else if (fPort == 30){ return {\"temperature\":((parseFloat(String.fromCharCode(bytes[0],bytes[1]))-30)), \"humidity\":(parseInt(String.fromCharCode(bytes[2],bytes[3]))+40) } }else if (fPort == 40){ return {\"voltage\":((parseFloat(String.fromCharCode(bytes[0],bytes[1]))+110)/10), \"impulse\":(parseInt(String.fromCharCode(bytes[2]))+25) }  } else if (fPort == 50){ return {\"status\":(parseFloat(String.fromCharCode(bytes[0],bytes[1]))) } }  ;  }",    "supports32BitFCnt": true,    "supportsClassB": true,    "supportsClassC": true,    "supportsJoin": true,   "tags": {} } }' 'localhost:8080/api/device-profiles' )

deviceProfileID=$(echo $OUTPUT  |   sed -e 's/.*id":"//' -e 's/".*//' -e 's/,//g')

# Wait one Sec for get deviceProfileID

#echo '(-X POST --header 'Content-Type:application/json' --header 'Accept: application/json' --header 'Grpc-Metadata-Authorization: Bearer '$jwtToken -d '{  "device": {    "applicationID": "'$applicationID'", "description": "WWH-Device",    "devEUI": "036b7539dd631e1b",    "deviceProfileID": "'$serviceProfileID'",    "isDisabled": false,    "name": "WWH-Device",    "referenceAltitude": 0,    "skipFCntCheck": true,    "tags": {},    "variables": {}  } }' 'localhost:8080/api/devices')'

#Add Device
OUTPUT=$(curl -X POST --header 'Content-Type:application/json' --header 'Accept: application/json' --header 'Grpc-Metadata-Authorization: Bearer '$jwtToken -d '{  "device": {    "applicationID": "'$applicationID'","description": "WWH-Device",    "devEUI": "'$DEVICE_EUI'",    "deviceProfileID": "'$deviceProfileID'",    "isDisabled": false,    "name": "WWH-Device-'$DEVICE_EUI'",    "referenceAltitude": 0,    "skipFCntCheck": true,    "tags": {},    "variables": {}  } }' 'localhost:8080/api/devices' )

#echo ${OUTPUT}

#Add Key

OUTPUT=$(curl -X POST --header 'Content-Type:application/json' --header 'Accept: application/json' --header 'Grpc-Metadata-Authorization: Bearer '$jwtToken -d '{"deviceKeys": { "nwkKey": "'$DEVICE_APP_KEY'", "devEUI": "'$DEVICE_EUI'"} }' 'localhost:8080/api/devices/'$DEVICE_EUI'/keys' )
#echo $OUTPUT |   sed -e 's/.*id":"//' -e 's/".*//' -e 's/,//g'  | read deviceProfileID

