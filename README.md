# WildWeide-Server

## Quick Start
Auf dem System muss Docker, docker-compose und git installiert sein.

1. Clone the repository: `git clone https://gitlab.com/lorawandd/wildweide-server.git`
2. `cd wildweide-server/`
3. Anpassen der Daten in `env.lorawan`
4. Starten `docker-compose up`

## Zielstellung
Zielstellung ist Sensor-Messdaten von der Basisstation einem fertig definierten Dashboard darzustellen. 

Folgende Dienste werden durch den Docker-Container bereit gestellt:
1. Chirpstack
2. Influxdb v2 mit Flux + Erweiterung Telegraf
3. Grafana

## Chirpstack

Die Weboberfläche ist nach Containerstart mit:

- URL: `http://localhost:8080/`
- Username: `admin`
- Passwort: `admin`

## Influxdb

Die Weboberfläche ist nach Containerstart mit:

- URL: `http://localhost:8087/`
- Username: `lorawanuser`
- Passwort: `eEqeojfr6irDvzC5GOftx7SxisMzI1DNRTrLI8iavvn7J9XrsnMfB`

aufrufbar.

Dabei wird ebenfalls das Bucket `lorawanbucket` sowie ein Token `lorawantoken` angelegt.

## Grafana

Die Weboberfläche ist nach Containerstart mit:

- URL: `http://localhost:3001/`
- Username: `admin`
- Passwort: `admin`

aufrufbar.

Gleichzeitig wird die Datasource zur Influxdb angelegt sowie das Dashboard.

Die eintreffenden Messdaten sind sofort im vorkonfigurierten Dashboard sichtbar.

![GPS-Tracker Dashboard](doc/gps_tracker.jpg)

## Funktionsweise

Mit dem mqtt-Consumer von Telegraf werden die eintreffenden Daten in die InfluxDB geschrieben und über das Grafana Dashboard zur Anzeige gebracht.

### Datenschema

Folgendes Datenschema/Json-Decoder für die Bezeichnung der Variablen sind zur sofortiger Anzeige notwendig.

fPort 10
- fence_tension

fPort 20
- latitude
- longitude

fPort 30
- temperature
- humidity

fPort 40
- voltage
- impulse

Natürlich ist es auch möglich das Dashboard mit weiteren Panels bzw. anderen Variablenbezeichnung zu erweitern.


## Überprüfung

Wenn Daten eingehen bzw. die Wildweide-Simulation gestartet ist, kann anschließend überprüft werden, ob Daten angekommen sind.

```
curl -XPOST "http://localhost:8087/api/v2/query?org=lorawan&bucket=lorawanbucket" -sS \
--header "Authorization: Token lorawantoken" \
-H 'Accept:application/csv' \
-H 'Content-type:application/vnd.flux' \
-d '
from(bucket:"lorawanbucket")
 |> range(start:-55m)  
 |> filter(fn: (r) => r["_field"] == "object_voltage")
'
```
